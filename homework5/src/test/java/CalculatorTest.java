import org.mockito.InOrder;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@Test
public class CalculatorTest {

    private Calculator calculator;
    private InOrder inOrder;
    private IOservice ioService;
    private final String BEFOR_FIRST_DIGIT = "Введите первое число или exit";
    private final String BEFOR_ACTION = "Введите нужное действие из четырех возможных: *, /, -, +   или exit";
    private final String BEFOR_SECOND_DIGIT = "Введите второе число или exit";

    @BeforeClass
    private void setUp() {
        ioService = mock(IOservice.class);
        calculator = new Calculator(ioService);
        inOrder = inOrder(ioService);
    }

    @DataProvider(name = "data-provider")
    public Object[][] dataProviderMethod() {
        return new Object[][]{{"3", "+", "5", "равно: 8"},
                {"35", "/", "10", "равно: 3.5"},
                {"24", "*", "10.6", "равно: 254.4"},
                {"1000", "-", "1", "равно: 999"}
        };
    }

    @Test(dataProvider = "data-provider")
    public void testMethod(String firstNum, String action, String secondNum, String result) {
        given(ioService.readString()).willReturn(firstNum);
        calculator.inPutFirstNum();
        inOrder.verify(ioService, times(1)).outPut(BEFOR_FIRST_DIGIT);
        inOrder.verify(ioService, times(1)).readString();
        given(ioService.readString()).willReturn(action);
        calculator.inPutAction();
        inOrder.verify(ioService, times(1)).outPut(BEFOR_ACTION);
        inOrder.verify(ioService, times(1)).readString();
        given(ioService.readString()).willReturn(secondNum);
        calculator.inPutSecondNum();
        inOrder.verify(ioService, times(1)).outPut(BEFOR_SECOND_DIGIT);
        inOrder.verify(ioService, times(1)).readString();
        calculator.calculation();
        inOrder.verify(ioService, times(1)).outPut(result);
    }

    @Test
    void enterWrongFirstNum() {
        given(ioService.readString()).willReturn("gdgdg");
        calculator.inPutFirstNum();
        inOrder.verify(ioService, times(1)).outPut(BEFOR_FIRST_DIGIT);
        inOrder.verify(ioService, times(1)).readString();
        inOrder.verify(ioService, times(1)).outPut("Не верный формат ввода");
    }

    @Test
    void enterWrongAction() {
        given(ioService.readString()).willReturn("--");
        calculator.inPutAction();
        inOrder.verify(ioService, times(1)).outPut(BEFOR_ACTION);
        inOrder.verify(ioService, times(1)).readString();
        inOrder.verify(ioService, times(1)).outPut("Не корректное действие");
    }

    @Test(expectedExceptions = NumberFormatException.class)
    void attemptDivideByZero() {
        given(ioService.readString()).willReturn("100");
        calculator.inPutFirstNum();
        inOrder.verify(ioService, times(1)).readString();
        given(ioService.readString()).willReturn("/");
        calculator.inPutAction();
        inOrder.verify(ioService, times(1)).readString();
        given(ioService.readString()).willReturn("0");
        calculator.inPutSecondNum();
        inOrder.verify(ioService, times(1)).readString();
        calculator.calculation();
    }
}
