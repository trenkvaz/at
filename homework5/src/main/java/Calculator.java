import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private double num1 = 0;
    private double num2 = 0;
    private String action = "";
    private IOservice iOservice;

    public Calculator(IOservice iOservice) {
        this.iOservice = iOservice;
    }

    public void start() {
        iOservice.outPut("Калькулятор включен.");
        while (true) {
            while (!inPutFirstNum()) ;
            while (!inPutAction()) ;
            while (!inPutSecondNum()) ;
            calculation();
        }
    }

    public boolean inPutFirstNum() {
        iOservice.outPut("Введите первое число или exit");
        num1 = 0;
        try {
            num1 = Double.parseDouble(getInPut());
        } catch (Exception e) {
            iOservice.outPut("Не верный формат ввода");
            return false;
        }
        return true;
    }

    public boolean inPutSecondNum() {
        iOservice.outPut("Введите второе число или exit");
        num2 = 0;
        try {
            num2 = Double.parseDouble(getInPut());
        } catch (Exception e) {
            iOservice.outPut("Не верный формат ввода");
            return false;
        }
        return true;
    }

    private String getInPut() {
        String res = iOservice.readString();
        if (res.equals("exit")) {
            System.exit(0);
        }
        return res;
    }

    public boolean inPutAction() {
        iOservice.outPut("Введите нужное действие из четырех возможных: *, /, -, +   или exit");
        action = getInPut();
        if (action.length() != 1) {
            iOservice.outPut("Не корректное действие");
            return false;
        } else {
            Pattern pattern = Pattern.compile("[*/\\-+]");
            Matcher matcher = pattern.matcher(action);
            if (!matcher.find()) {
                iOservice.outPut("Не корректное действие");
                return false;
            }
        }
        return true;
    }

    public void calculation() {
        double res = 0;
        if (action.equals("+")) res = num1 + num2;
        if (action.equals("*")) res = num1 * num2;
        if (action.equals("-")) res = num1 - num2;
        if (action.equals("/")) res = num1 / num2;
        if (res % 1 == 0) {
            iOservice.outPut("равно: " + (int) res);
        } else iOservice.outPut("равно: " + BigDecimal.valueOf(res).setScale(10, RoundingMode.HALF_UP).doubleValue());

    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator(new CalculatorIO());
        calculator.start();
    }
}
