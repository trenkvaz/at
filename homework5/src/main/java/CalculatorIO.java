import java.io.PrintStream;
import java.util.Scanner;

public class CalculatorIO implements IOservice {

    private final PrintStream out;
    private final Scanner in;

    public CalculatorIO() {
        out = System.out;
        in = new Scanner(System.in);
    }

    @Override
    public String readString() {
        return in.nextLine();
    }

    @Override
    public void outPut(String output) {
        out.println(output);
    }
}
