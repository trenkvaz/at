import model.Kotik;

public class Application {

    public static void main(String[] args) {
        Kotik kotik1 = new Kotik(3, "Vasya", 4, "mur");
        Kotik kotik2 = new Kotik();
        kotik2.setKotik(5, "Murka", 2, "mayu");
        System.out.println("One day of " + kotik1.getName() + " (weight-" + kotik1.getWeight() + ", prettiness-" + kotik1.getPrettiness() + ")");
        kotik1.liveAnotherDay();
        System.out.println();
        System.out.println("One day of " + kotik2.getName() + " (weight-" + kotik2.getWeight() + ", prettiness-" + kotik2.getPrettiness() + ")");
        kotik2.liveAnotherDay();
        System.out.println();
        System.out.println("Do these cats speak the same way? " + kotik1.getMeow().equals(kotik2.getMeow()));
        System.out.println("We have " + Kotik.getCountKotik() + " cats");
    }
}
