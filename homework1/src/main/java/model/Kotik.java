package model;

public class Kotik {

    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private int satiety = 1;
    private static int countKotik = 0;

    public Kotik() {
        countKotik++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        countKotik++;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    private boolean play() {
        if (satiety < 1) return false;
        System.out.println(name+" is playing");
        satiety--;
        return true;
    }

    private boolean sleep() {
        if (satiety < 1) return false;
        System.out.println(name+" is sleeping");
        satiety--;
        return true;
    }

    private boolean chaseMouse() {
        if (satiety < 1) return false;
        System.out.println(name+" is chasing a mouse");
        satiety--;
        return true;
    }

    private boolean walk() {
        if (satiety < 1) return false;
        System.out.println(name+" is walking");
        satiety--;
        return true;
    }

    private boolean watchBirds(){
        if (satiety < 1) return false;
        System.out.println(name+" is watching birds");
        satiety--;
        return true;
    }

    private void eat(int satiety) {
        this.satiety += satiety;
        System.out.println(name+" is eating");
    }

    private void eat(int satiety, String foodName) {
        this.satiety += satiety;
        System.out.println(name+" is eating " + foodName);
    }

    private void eat() {
        eat(4, "meat");
    }

    public static int getCountKotik() {
        return countKotik;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public void liveAnotherDay() {

        for (int i = 0; i < 24; i++) {
            int r = (int) (Math.random() * 5 + 1);
            boolean doSomething = false;
            if (r == 1) doSomething = play();
            if (r == 2) doSomething = sleep();
            if (r == 3) doSomething = chaseMouse();
            if (r == 4) doSomething = walk();
            if (r == 5) doSomething = watchBirds();
            if (doSomething) continue;
            eat();
        }
    }

}
