package animals;

import interfaces.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish() {
        name = "Fish";
    }

    public void swim() {
        System.out.println("Fish is swimming");
    }
}
