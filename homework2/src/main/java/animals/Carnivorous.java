package animals;

import food.Food;
import food.Grass;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public void eat(Food food) {
        if (food instanceof Meat) System.out.println(name + " is eating meat");
        else System.out.println(name + " won't eat grass");
    }
}
