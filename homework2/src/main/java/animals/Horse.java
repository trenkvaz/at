package animals;

import interfaces.Run;
import interfaces.Voice;

public class Horse extends Herbivore implements Voice, Run {

    public Horse() {
        name = "Horse";
    }

    public String voice() {
        return "Horse is neighing";
    }

    public void run() {
        System.out.println("Horse is running");
    }
}
