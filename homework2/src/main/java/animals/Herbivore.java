package animals;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public void eat(Food food) {
        if (food instanceof Grass) System.out.println(name + " is eating grass");
        else System.out.println(name + " won't eat meat");
    }
}
