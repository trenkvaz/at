package animals;

import interfaces.Fly;
import interfaces.Swim;
import interfaces.Voice;

public class Duck extends Herbivore implements Voice, Fly, Swim {

    public Duck() {
        name = "Duck";
    }

    public String voice() {
        return "Duck is quacking";
    }

    public void fly() {
        System.out.println("Duck is flying");
    }

    public void swim() {
        System.out.println("Duck is swimming");
    }
}
