package animals;

import interfaces.Run;
import interfaces.Voice;

public class Wolf extends Carnivorous implements Voice, Run {

    public Wolf() {
        name = "Wolf";
    }

    public String voice() {
        return "Wolf is howling";
    }

    public void run() {
        System.out.println("Wolf is running");
    }
}
