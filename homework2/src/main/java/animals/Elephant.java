package animals;

import interfaces.Run;
import interfaces.Voice;

public class Elephant extends Herbivore implements Voice, Run {

    public Elephant() {
        name = "Elephant";
    }

    public String voice() {
        return "Elephant is trumpeting";
    }

    public void run() {
        System.out.println("Elephant is running");
    }
}
