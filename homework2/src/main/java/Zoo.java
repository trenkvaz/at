import animals.*;
import food.Food;
import food.Grass;
import food.Meat;
import interfaces.Swim;
import interfaces.Voice;

public class Zoo {

    public static void main(String[] args) {
        Worker worker = new Worker();
        Food[] foods = {new Grass("hay"), new Meat("pork")};
        Animal[] animals = {new Fish(), new Duck(), new Horse(), new Elephant(), new Wolf(), new Lion()};
        for (Animal animal : animals) {
            for (Food food : foods)
                worker.feed(animal, food);
        }
        Voice[] animalsWithVoices = {new Duck(), new Horse(), new Elephant(), new Wolf(), new Lion()};
        for (Voice animal : animalsWithVoices) {
            worker.getVoice(animal);
        }
        Swim[] pond = {new Fish(), new Duck(), new Fish(), new Duck()};
        for (Swim swimming : pond)
            swimming.swim();
    }
}
