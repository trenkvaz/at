1. Вывести список самолетов с кодами 320, 321, 733;
SELECT ad.MODEL, ad.AIRCRAFT_CODE  FROM LANIT.AIRCRAFTS_DATA ad  
WHERE  ad.AIRCRAFT_CODE = '320' OR ad.AIRCRAFT_CODE = '321' OR ad.AIRCRAFT_CODE = '733';


2. Вывести список самолетов с кодом не на 3;
SELECT ad.MODEL, ad.AIRCRAFT_CODE  FROM LANIT.AIRCRAFTS_DATA ad  
WHERE ad.AIRCRAFT_CODE NOT LIKE '3%' ;


3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT *
FROM LANIT.TICKETS t  
WHERE  t.PASSENGER_NAME LIKE 'OLGA%' AND (t.EMAIL LIKE 'olga%' OR t.EMAIL is NULL)


4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
SELECT ad.MODEL, ad.range
FROM LANIT.AIRCRAFTS_DATA ad 
WHERE ad.range = 5600 OR ad.range = 5700
ORDER BY ad.RANGE DESC 


5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
SELECT a.AIRPORT_NAME, a.CITY  FROM LANIT.AIRPORTS_DATA a
WHERE a.CITY = 'Moscow'
ORDER BY a.AIRPORT_NAME



6. Вывести список всех городов без повторов в зоне «Europe»;
SELECT a.CITY FROM LANIT.AIRPORTS_DATA a
WHERE a.TIMEZONE LIKE 'Europe%'
GROUP BY a.CITY  


7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%;
SELECT b.TOTAL_AMOUNT-b.TOTAL_AMOUNT*0.1
FROM LANIT.BOOKINGS b
WHERE b.BOOK_REF LIKE '3A4%'


8. Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида «Данные по месту: номер места 1», 
«Данные по месту: номер места 2» … и тд;
SELECT CONCAT('Seat data: seat number ',s.SEAT_NO)
FROM LANIT.SEATS s 
WHERE s.AIRCRAFT_CODE = '320' AND s.FARE_CONDITIONS = 'Business'


9. Найти максимальную и минимальную сумму бронирования в 2017 году;
SELECT max(b.TOTAL_AMOUNT), min(b.TOTAL_AMOUNT)
FROM LANIT.BOOKINGS b
WHERE TRUNC(b.BOOK_DATE ,'YYYY') = '01.01.2017'


10. Найти количество мест во всех самолетах, вывести в разрезе самолетов;
SELECT s.AIRCRAFT_CODE, count(s.SEAT_NO)
FROM LANIT.SEATS s
GROUP BY s.AIRCRAFT_CODE 



11. Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест; 
SELECT ad.MODEL, s.FARE_CONDITIONS, count(s.SEAT_NO) AS amount_seats 
FROM LANIT.SEATS s, LANIT.AIRCRAFTS_DATA ad 
WHERE s.AIRCRAFT_CODE = ad.AIRCRAFT_CODE 
GROUP BY ad.MODEL , s.FARE_CONDITIONS
ORDER BY ad.MODEL, s.FARE_CONDITIONS



12. Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
SELECT *
FROM LANIT.TICKETS t  
WHERE  t.PASSENGER_NAME LIKE 'ALEKSANDR STEPANOV%' AND t.PHONE LIKE '%11'



13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
SELECT t.PASSENGER_NAME, COUNT(*) AS count_tickets
FROM LANIT.TICKETS t  
WHERE  t.PASSENGER_NAME LIKE 'ALEKSANDR%'
GROUP BY t.PASSENGER_NAME
HAVING COUNT(*)>2000
ORDER BY COUNT(*) DESC



14. Вывести дни в сентябре 2017 с количеством рейсов больше 500.;
SELECT TRUNC(f.DATE_DEPARTURE ,'DDD')
FROM LANIT.FLIGHTS f
WHERE TRUNC(f.DATE_DEPARTURE ,'MONTH') = '01.09.2017'
GROUP BY TRUNC(f.DATE_DEPARTURE ,'DDD')
HAVING COUNT(*)>500



15. Вывести список городов, в которых несколько аэропортов;
SELECT ad.CITY, COUNT(*) AS airports  FROM LANIT.AIRPORTS_DATA ad
GROUP BY ad.CITY 
HAVING COUNT(*)>1



16. Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата;
SELECT ad.MODEL, count(s.SEAT_NO) AS amount_seats
FROM LANIT.SEATS s, LANIT.AIRCRAFTS_DATA ad 
WHERE s.AIRCRAFT_CODE = ad.AIRCRAFT_CODE 
GROUP BY s.AIRCRAFT_CODE, ad.MODEL 



17. Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017;
SELECT f.* FROM LANIT.AIRPORTS_DATA ad2, LANIT.FLIGHTS f 
WHERE ad2.CITY = 'Moscow'  AND ad2.AIRPORT_CODE = f.DEPARTURE_AIRPORT  AND 
TRUNC(f.DATE_DEPARTURE  ,'MONTH') = '01.09.2017'



18. Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017;
SELECT f.DEPARTURE_AIRPORT, count(f.FLIGHT_ID) AS amount_flights FROM LANIT.AIRPORTS_DATA ad, LANIT.FLIGHTS f 
WHERE ad.CITY = 'Moscow'  AND ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT  AND 
TRUNC(f.DATE_DEPARTURE  ,'YEAR') = '01.01.2017'
GROUP BY f.DEPARTURE_AIRPORT



19. Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017;
SELECT f.DEPARTURE_AIRPORT, TRUNC(f.DATE_DEPARTURE  ,'MONTH') AS months, count(f.FLIGHT_ID) AS amount_flights FROM LANIT.AIRPORTS_DATA ad, LANIT.FLIGHTS f 
WHERE ad.CITY = 'Moscow'  AND ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT  AND 
TRUNC(f.DATE_DEPARTURE  ,'YEAR') = '01.01.2017'
GROUP BY f.DEPARTURE_AIRPORT, TRUNC(f.DATE_DEPARTURE  ,'MONTH')
ORDER BY f.DEPARTURE_AIRPORT, TRUNC(f.DATE_DEPARTURE  ,'MONTH')



20. Найти все билеты по бронированию на «3A4B»;
SELECT *
FROM LANIT.BOOKINGS b
WHERE b.BOOK_REF LIKE '3A4B%'



21. Найти все перелеты по бронированию на «3A4B»;
SELECT *
FROM LANIT.TICKETS t 
WHERE t.BOOK_REF LIKE '3A4B%'





