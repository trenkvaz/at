import animals.*;
import food.Food;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import interfaces.Swim;
import interfaces.Voice;

public class Zoo {

    public static void main(String[] args) throws WrongFoodException {
        Worker worker = new Worker();
        Aviary<Carnivorous> carnivorousAviary = new Aviary<>(SizeAviary.Middle);
        Wolf wolf1 = new Wolf("wolf1");
        Wolf wolf2 = new Wolf("wolf2");
        carnivorousAviary.addAnimalToAviary(wolf1);
        carnivorousAviary.addAnimalToAviary(wolf2);
        System.out.println(carnivorousAviary.getAnimal("wolf1").getName() + " obtained from an aviary");
        carnivorousAviary.removeAnimalFromAviary(wolf2);
        Aviary<Herbivore> herbivoreAviary = new Aviary<>(SizeAviary.Small);
        Duck duck1 = new Duck("duck1");
        Horse horse1 = new Horse("horse2");
        herbivoreAviary.addAnimalToAviary(duck1);
        herbivoreAviary.addAnimalToAviary(horse1);
        Food[] foods = {new Grass("hay"), new Meat("pork")};
        Animal[] animals = {new Fish("fish1"), new Duck("duck1"), new Horse("horse1"),
                new Elephant("elephant1"), new Wolf("wolf1"), new Lion("lion1")};
        for (Animal animal : animals) {
            for (Food food : foods)
                worker.feed(animal, food);
        }
    }
}
