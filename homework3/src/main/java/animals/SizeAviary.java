package animals;

public enum SizeAviary {
    Small,
    Middle,
    Big,
    VeryBig
}
