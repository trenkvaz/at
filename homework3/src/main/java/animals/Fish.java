package animals;

import interfaces.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name) {
        super.name = name;
        sizeAviary = SizeAviary.Small;
    }

    public void swim() {
        System.out.println("Fish is swimming");
    }
}
