package animals;

import food.Food;
import food.WrongFoodException;

public abstract class Animal {

    protected String name;
    protected SizeAviary sizeAviary;

    public abstract void eat(Food food) throws WrongFoodException;

    public SizeAviary getSizeAviary() {
        return sizeAviary;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return (obj.hashCode() == this.hashCode());
    }
}
