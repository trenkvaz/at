package animals;

import food.Food;
import food.Grass;
import food.Meat;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {

    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) System.out.println(name + " is eating meat");
        else throw new WrongFoodException();
    }
}
