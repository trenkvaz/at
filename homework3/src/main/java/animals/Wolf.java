package animals;

import interfaces.Run;
import interfaces.Voice;

public class Wolf extends Carnivorous implements Voice, Run {

    public Wolf(String name) {
        super.name = name;
        sizeAviary = SizeAviary.Middle;
    }

    public String voice() {
        return "Wolf is howling";
    }

    public void run() {
        System.out.println("Wolf is running");
    }
}
