package animals;

import interfaces.Run;
import interfaces.Voice;

public class Lion extends Carnivorous implements Voice, Run {

    public Lion(String name) {
        super.name = name;
        sizeAviary = SizeAviary.Small;
    }

    public String voice() {
        return "Lion is roaring";
    }

    public void run() {
        System.out.println("Lion is running");
    }
}
