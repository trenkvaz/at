package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {

    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) System.out.println(name + " is eating grass");
        else throw new WrongFoodException();
    }
}
