import animals.*;

import java.util.HashSet;
import java.util.Set;

public class Aviary<T extends Animal> {

    public Set<T> setAviary = new HashSet<>();
    private SizeAviary sizeAviary;

    public Aviary(SizeAviary size) {
        sizeAviary = size;
    }

    public void addAnimalToAviary(T animal) {
        boolean isHoused = false;
        switch (animal.getSizeAviary()) {
            case Small:
                setAviary.add(animal);
                isHoused = true;
                break;
            case Middle:
                if (sizeAviary != SizeAviary.Small) {
                    setAviary.add(animal);
                    isHoused = true;
                }
                break;
            case Big:
                if (sizeAviary != SizeAviary.Small && sizeAviary != SizeAviary.Middle) {
                    setAviary.add(animal);
                    isHoused = true;
                }
                break;
            case VeryBig:
                if (sizeAviary == SizeAviary.VeryBig) {
                    setAviary.add(animal);
                    isHoused = true;
                }
                break;
        }
        if (isHoused) System.out.println(animal.getName() + " is housed in the aviary");
        else System.out.println(animal.getName() + " is too big for the aviary");
    }

    public void removeAnimalFromAviary(Animal animal) {
        setAviary.remove(animal);
    }

    public Animal getAnimal(String name) {
        for (Animal animal : setAviary) {
            if (name.hashCode() == animal.hashCode())
                return animal;
        }
        return null;
    }
}
